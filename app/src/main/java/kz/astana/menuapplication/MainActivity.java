package kz.astana.menuapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST_CODE = 100;
    private final int MENU_SHOW = 10;
    private final int MENU_REMOVE = 20;
    private MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myAdapter = new MyAdapter(getCountriesList());

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recyclerView.setAdapter(myAdapter);

        myAdapter.setOnClickListener(new MyAdapter.MyOnClickListener() {
            @Override
            public void onClick(Country country) {
                Toast.makeText(MainActivity.this, country.getName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(Country country) {
                myAdapter.removeItem(country);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        registerForContextMenu(fab);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(Menu.NONE, MENU_SHOW, 1, "Show");
        menu.add(Menu.NONE, MENU_REMOVE, 2, "Remove");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.create_activity:
                Intent i = new Intent(MainActivity.this, EmptyActivity.class);
                startActivity(i);
                return true;
            case R.id.create_item:
                Intent intent = new Intent(MainActivity.this, CreateActivity.class);
                startActivityForResult(intent, REQUEST_CODE);
                return true;
            case R.id.exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            int resId = data.getIntExtra("EXTRA_IMAGE", R.drawable.ic_kazakhstan);
            String name = data.getStringExtra("EXTRA_NAME");
            String capital = data.getStringExtra("EXTRA_CAPITAL");

            myAdapter.addItem(new Country(resId, name, capital));
        }
    }

    private ArrayList<Country> getCountriesList() {
        ArrayList<Country> countries = new ArrayList<Country>();
        countries.add(new Country(R.drawable.ic_kazakhstan, "Kazakhstan", "Nur-Sultan"));
        countries.add(new Country(R.drawable.ic_albania, "Albania", "Tirana"));

        return countries;
    }
}