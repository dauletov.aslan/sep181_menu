package kz.astana.menuapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class CreateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        Spinner spinner = findViewById(R.id.spinner);
        SpinnerAdapter adapter = new SpinnerAdapter(getResIds());
        spinner.setAdapter(adapter);

        EditText countryName = findViewById(R.id.countryNameEditText);
        EditText countryCapital = findViewById(R.id.countryCapitalEditText);

        Button button = findViewById(R.id.sendButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();

                int position = spinner.getSelectedItemPosition();

                intent.putExtra("EXTRA_IMAGE", (int) adapter.getItem(position));
                intent.putExtra("EXTRA_NAME", countryName.getText().toString());
                intent.putExtra("EXTRA_CAPITAL", countryCapital.getText().toString());

                setResult(Activity.RESULT_OK, intent);
                onBackPressed();
            }
        });
    }

    private ArrayList<Integer> getResIds() {
        ArrayList<Integer> resIds = new ArrayList<Integer>();
        resIds.add(R.drawable.ic_argentina);
        resIds.add(R.drawable.ic_bolivia);
        resIds.add(R.drawable.ic_china);

        return resIds;
    }
}