package kz.astana.menuapplication;

public class Country {
    private int flagResId;
    private String name, capital;

    public Country(int flagResId, String name, String capital) {
        this.flagResId = flagResId;
        this.name = name;
        this.capital = capital;
    }

    public int getFlagResId() {
        return flagResId;
    }

    public void setFlagResId(int flagResId) {
        this.flagResId = flagResId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }
}
