package kz.astana.menuapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private ArrayList<Country> countries;
    private MyOnClickListener onClickListener;

    public MyAdapter(ArrayList<Country> countries) {
        this.countries = countries;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Country country = countries.get(position);

        holder.flag.setImageResource(country.getFlagResId());
        holder.name.setText(country.getName());
        holder.capital.setText(country.getCapital());

        if (onClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(country);
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    onClickListener.onLongClick(country);
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public void setOnClickListener(MyOnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void addItem(Country country) {
        countries.add(country);
        notifyDataSetChanged();
    }

    public void removeItem(Country country) {
        countries.remove(country);
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView flag;
        public TextView name, capital;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            flag = itemView.findViewById(R.id.countryFlagImageView);
            name = itemView.findViewById(R.id.countryNameTextView);
            capital = itemView.findViewById(R.id.countryCapitalTextView);
        }
    }

    public interface MyOnClickListener {
        void onClick(Country country);

        void onLongClick(Country country);
    }
}